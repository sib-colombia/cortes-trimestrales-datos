# Archivo público
Ahora puede enontrar estos mismos datos en una [carpeta en Drive](https://drive.google.com/drive/folders/1Dx92OW30FHqkhqaSAtaVT9S7QiI_ixoE). Esta carpeta contendrá los datos desde el cuarto trimestre del 2018 hasta la fecha actual.

# Histórico de datos trimestrales
> Este repositorio no se actualiza

Este repositorio solía contener los datos consolidados en este repositorio corresponden a registros biológicos publicados a través del Sistema de Información sobre Biodiversidad de Colombia - SiB Colombia, por la red de socios publicadores y los datos repatriados de GBIF, eBird, Xeno - Canto y iNaturalist. El número de registros biológicos varía según la fecha de corte de los datos, para lo cual se realiza un consolidado de datos de forma trimestral.

Los datos a nivel nacional en este repositorio son generados trimestralmente por el Equipo Coordinador del SiB Colombia a través de un script desarrollado en Python (3.9). La síntesis se obtiene al cruzar los registros biológicos con otros insumos de información geográfica y temática (ver detalle en ficha metodológica).

*La versión actual (en línea) de Biodiversidad en cifras corresponde al archivo Trimestre IV de 2021


## Cómo citar los datos
SiB Colombia (año, mes día) Reporte Trimestral de Datos – Mes Año, Sistema de Información sobre Biodiversidad de Colombia. Recuperado de: https://gitlab.com/sib-colombia/cortes-trimestrales-datos

*Ejemplo*:

SiB Colombia (2020, abril 10) Reporte Trimestral de Datos – Marzo 2020, Sistema de Información sobre Biodiversidad de Colombia. Recuperado de: https://gitlab.com/sib-colombia/cortes-trimestrales-datos


## Elementos del conjunto de datos
Las columnas que encontrará en el archivo de descarga corresponden a:

Elementos del estándar Darwin Core. Las definiciones de cada elemento las puede encontrar en el siguiente [enlace](https://biodiversidad.co/compartir/estandar-darwin-core/).

'occurrenceID',  'basisOfRecord', 'institutionCode',  'collectionCode',  'catalogNumber',  'type',    'occurrenceRemarks', 'recordedBy',  'individualCount',  'sex',  'eventID',  'samplingProtocol',  'samplingEffort',  'eventDate',  'year',  'month',  'day',  'habitat',  'continent',  'waterBody',  'country',  'countryCode',  'stateProvince', 'county', 'municipality',  'locality', 'minimumElevationInMeters',  'maximumElevationInMeters',  'minimumDepthInMeters',  'maximumDepthInMeters',  'locationRemarks',  'decimalLatitude',  'decimalLongitude',  'geodeticDatum',  'coordinateUncertaintyInMeters',  'taxonID',  'scientificName',  'kingdom',  'phylum',  'class',  'order',  'family',  'genus',  'specificEpithet',  'infraspecificEpithet',  'taxonRank',  'scientificNameAuthorship', 'identifiedBy', 'dateIdentified',  'previousIdentifications'.  


Elementos obtenidos a partir del llamado al API de GBIF

+ *gbifID*: Identificador del registro en la base de datos de GBIF (identificador único).
+ *publishingOrgKey*: Identificador único de la organización en el Portal de Datos de GBIF.
+ *datasetKey*: Identificador único del conjunto de datos en el Portal de Datos de GBIF.
+ *datasetID*: Enlace basado en el identificador único del conjunto de datos en el Portal de Datos de GBIF.
+ *datasetName*: Nombre del conjunto de datos publicado para cada registros biológico en el archivo.
+ *dataType*: Tipo de datos de la publicación original (Eventos, registros biológicos).
+ *organization*: Nombre de la organización publicadora del registro.
+ *doi*: Enlace a la publicación de donde proviene el registro.
+ *license*: Licencia de uso de datos (Creative Commons) del conjunto de datos al cual pertenece el registro.
+ *created*: Fecha de la primera publicación del registro.
+ *publishingCountry:* Codificación Iso 3166-1 del país de origen de la publicación del registro.
+ *species*: Nombre científico aceptado según el árbol taxonómico de GBIF, interpretado a partir del elemento original scientificName.
+ *previousIdentifications*: en este elemento se mapea el nombre científico (*scientificName*) original del registro biológico en la publicación.
+ *bibliographicCitation*: Enlace del registro al Portal de datos de GBIF.

Elementos adicionales agregados a partir de listas de referencia nacionales y listas externas como CITES y UICN.

+ *appendixCITES*: Categoría de amenaza de acuerdo a la lista de especies objeto de comercio establecida por la Convención sobre el Comercio Internacional de Especies Amenazadas de Fauna y Flora Silvestres (CITES)  a fecha de corte de 2020-12-31.
+ *threatStatus*: Estado de amenaza de acuerdo a la lista de especies amenazadas de Colombia, de la Resolución 1912 de 2017 (MADS, 2018), expedida por el Ministerio de Ambiente y Desarrollo Sostenible.
+ *threatStatus_UICN*: Estado de amenaza de acuerdo a las categorías de la Unión Internacional para la Conservación de la Naturaleza (UICN) a fecha de corte de 2019-12-31.
+ *endemic*| endemic: Identifica si el registro corresponde a una especie endémica de acuerdo a las listas de referencia para el país, donde se obtiene información sobre el endemismo de aves, peces dulceacuícolas, mamíferos, líquenes y plantas; otros grupos serán añadidos a medida que se publiquen nuevas listas de referencia para el país.
+ *migratory*: Identifica si el registro corresponde a una especie migratoria. Aplica para especies migratorias de aves únicamente, otros grupos serán añadidos a medida que se publiquen nuevas listas de referencia para el país.
+ *exotic*: Identifica si el registro corresponde a una especie exótica de acuerdo la lista oficial de especies exóticas para el país, que incluye únicamente información curada para animales.
+ *isInvasive_griis*: Identifica si el registro corresponde a una especie invasora de acuerdo la lista oficial de especies exóticas para el país, que incluye únicamente información curada para animales.
+ *isInvasive_mads*: Identifica si el registro corresponde a una especie invasora de acuerdo a la Resolución 207 de 2010 expedida por el Ministerio de Ambiente y Desarrollo Sostenible.
+ *Departamento-ubicaciónCoordenada*: Departamento con el cual coincide la coordenada reportada para el registro de acuerdo al cruce geográfico con la capa de límites departamentales y municipales (IGAC 2014,1:25.000).
+ *Municipio-ubicaciónCoordenada*: Municipio con el cual coincide la coordenada reportada para el registro de acuerdo al cruce geográfico con la capa de límites departamentales y municipales (IGAC 2014, 1:25.000).
+ *taxoTematicChecklist*: Acrónimo que referencia la lista de especies (de referencia o externa) de donde se extrae la información temática de la especie.


## Recomendaciones para la interpretación de los datos

Para facilitar la consolidación de cifras a partir de una clasificación taxonómica común, los datos adjuntos tienen una interpretación de calidad a partir del árbol taxonómico de GBIF.


+ Tener en cuenta que los problemas de calidad de los datos geográficos pueden generar discrepancias entre la ubicación documentada y la ubicación real de las coordenadas.


+ Los datos compartidos en esta consulta son aportados por diferentes entidades nacionales e internacionales. Es importante que cada una reciba la atribución correspondiente frente al uso que se vaya a hacer de esta información. Para ello se incluyeron los elementos datasetName, organization y doi, donde identificará el nombre del conjunto de datos publicados, la organización publicadora y el enlace al recurso, respectivamente.


+ Los datos compartidos a través del SiB Colombia se encuentran bajo el uso de licencias Creative Commons. Para cada registro encontrará la licencia de uso que se determinó en la publicación. Esto determinará la manera en que se puede y debe dar uso a esta información. Para mayor información sobre las licencias creative commons diríjase a la wiki de publicación del SiB Colombia.


+ Los cortes de datos pueden tener modificaciones para su mejora, si tiene dudas sobre el contenido de algun elemento por favor comuniquese con nosotros a través del correo sib@humboldt.org.co.



